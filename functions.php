<?php
/**
 * scrawk functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package scrawk
 */

if ( ! function_exists( 'scrawk_setup' ) ) :

	function scrawk_setup() {

		load_theme_textdomain( 'scrawk', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'scrawk' ),
			'servicemenu' => esc_html__( 'Service Menu', 'scrawk' ),
			'mobilemenu' => esc_html__( 'Mobile Menu', 'scrawk' ),
			'footer_links' => esc_html__( 'Footer Links', 'scrawk' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'scrawk_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'scrawk_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function scrawk_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'scrawk_content_width', 640 );
}
add_action( 'after_setup_theme', 'scrawk_content_width', 0 );


function scrawk_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'scrawk' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'scrawk' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'scrawk_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function scrawk_scripts() {
	wp_enqueue_style('scrawk-bootstrap-style', get_stylesheet_directory_uri().'/vendors/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('scrawk-bootstrap-theme-style', get_stylesheet_directory_uri().'/vendors/bootstrap/css/bootstrap-theme.min.css');
	wp_enqueue_style('scrawk-fa-style', get_stylesheet_directory_uri().'/vendors/fonts/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('scrawk-ham-style', get_stylesheet_directory_uri().'/vendors/hamburgers/hamburgers.css');
	wp_enqueue_style('scrawk-custom-style', get_stylesheet_directory_uri().'/assets/css/scrawk.css');

	wp_enqueue_script( 'scrawk-bootstrap-script', get_stylesheet_directory_uri() . '/vendors/bootstrap/js/bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'scrawk-custom-script', get_stylesheet_directory_uri() . '/assets/js/scrawk.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'scrawk_scripts' );


/**
 * Load Admin styles
*/

function scrawk_load_admin_style() {
    wp_enqueue_style( 'scrawk_admin_css', get_stylesheet_directory_uri() . '/assets/css/scrawk-admin.css');
    wp_enqueue_style( 'scrawk_admin_fa_css', get_stylesheet_directory_uri() . '/vendors/fonts/font-awesome/css/font-awesome.min.css');
}
add_action( 'admin_enqueue_scripts', 'scrawk_load_admin_style' );

/**
 * scrawk Custom header hook
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * scrawk Template tags hook
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * scrawk Template functions hook.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * scrawk Template Custom Post types.
 */
require get_template_directory() . '/inc/template-cpt.php';

/**
 * scrawk Plugin Activation Class
 */
require_once locate_template('/lib/scrawk-plugin-activation/class-scrawk-plugin-activation.php');
require get_template_directory() . '/lib/scrawk-plugin-activation/template-required-plugin.php';

/**
 * scrawk Template Options hook
 */
require get_template_directory() . '/lib/scrawk-options-framework/template-options.php';



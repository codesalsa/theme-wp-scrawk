<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package scrawk
 */

?>

<footer class="full-width clearfix" data-bg="black">
    <div class="container" data-padding="5015">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 f-item">
                <?php if ( of_get_option('logo_uploader') ) { ?> 

                    <div class="footer-branding"><img src="<?php echo of_get_option('footer_logo_uploader'); ?>" class="img-responsive" /></div>

                <?php } ?>
                <p>Excellence decisively nay man yet impression for contrasted remarkably. There spoke happy for you are out. Fertile how old address did showing because sitting replied six. Had arose guest visit going off child she new.</p>
            </div>
            <?php dynamic_sidebar( 'Footer Top Widgets' ); ?>
        </div>
        <div class="row no-padding-bottom" data-padding="2500">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 copyright">
                <?php if ( of_get_option('copyright_text') ): ?>
                    <p><?php echo of_get_option('copyright_text'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 socials">
            <ul class="mom-social-icons">
                <?php if ( of_get_option('social_fb') ) { ?>
                    <li class="facebook">
                        <a target="_blank" href="<?php echo of_get_option('social_fb'); ?>"><i class="fa fa-facebook"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_twitter') ) { ?>
                    <li class="twitter">
                        <a target="_blank" href="<?php echo of_get_option('social_twitter'); ?>"><i class="fa fa-twitter"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_linkedin') ) { ?>
                    <li class="linkedin">
                        <a target="_blank" href="<?php echo of_get_option('social_linkedin'); ?>"><i class="fa fa-linkedin"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_gplus') ) { ?>
                    <li class="gplus">
                        <a target="_blank" href="<?php echo of_get_option('social_gplus'); ?>"><i class="fa fa-google-plus"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_youtube') ) { ?>
                    <li class="youtube">
                        <a target="_blank" href="<?php echo of_get_option('social_youtube'); ?>"><i class="fa fa-youtube"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_pintrest') ) { ?>
                    <li class="pinterest">
                        <a target="_blank" href="<?php echo of_get_option('social_pintrest'); ?>"><i class="fa fa-pinterest"></i></a>
                    </li>
                <?php } ?>
                <?php if ( of_get_option('social_instagram') ) { ?>
                    <li class="instagram">
                        <a target="_blank" href="<?php echo of_get_option('social_instagram'); ?>"><i class="fa fa-instagram"></i></a>
                    </li>
                <?php } ?>
            </ul>
            </div>
        </div>
    </div>
</footer>


<?php wp_footer(); ?>

</body>
</html>

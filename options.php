<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_framework_theme'),
		'two' => __('Two', 'options_framework_theme'),
		'three' => __('Three', 'options_framework_theme'),
		'four' => __('Four', 'options_framework_theme'),
		'five' => __('Five', 'options_framework_theme')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_framework_theme'),
		'two' => __('Pancake', 'options_framework_theme'),
		'three' => __('Omelette', 'options_framework_theme'),
		'four' => __('Crepe', 'options_framework_theme'),
		'five' => __('Waffle', 'options_framework_theme')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

	$options[] = array(
		'name' => __('General', 'options_framework_theme'),
		'type' => 'heading',
		'icon' => 'bullseye');

	$options[] = array(
		'name' => __('Upload Logo', 'options_framework_theme'),
		'desc' => __('Upload your Themes Logo file', 'options_framework_theme'),
		'id' => 'logo_uploader',
		'type' => 'upload');
	
	$options[] = array(
		'name' => __('Upload Favicon', 'options_framework_theme'),
		'desc' => __('Upload your Themes Favicon file', 'options_framework_theme'),
		'id' => 'favicon_uploader',
		'type' => 'upload');
	
	$options[] = array(
		'name' => __('SEO', 'options_framework_theme'),
		'type' => 'heading',
		'icon' => 'bar-chart');
	
	$options[] = array(
		'name' => __('Custom Site Title', 'options_framework_theme'),
		'desc' => __('A custom site title that will override the existing Site title of Wordpress Settings', 'options_framework_theme'),
		'id' => 'site_title',
		'placeholder' => 'Enter a Custom Site Title',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Custom Site Keywords', 'options_framework_theme'),
		'desc' => __('Custom site Keywords that will help in building SEO of the website', 'options_framework_theme'),
		'id' => 'site_keywords',
		'placeholder' => 'Enter Custom Site Keywords for SEO',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Custom Site Description', 'options_framework_theme'),
		'desc' => __('Custom site Description that will help in building SEO of the website', 'options_framework_theme'),
		'id' => 'site_description',
		'placeholder' => 'Enter Custom Site Description for SEO',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => __('Footer', 'options_framework_theme'),
		'type' => 'heading',
		'icon' => 'th-large');
		
	$options[] = array(
		'name' => __('Upload Footer Logo', 'options_framework_theme'),
		'desc' => __('Upload your Themes Footer Logo file', 'options_framework_theme'),
		'id' => 'footer_logo_uploader',
		'type' => 'upload');
	
	$options[] = array(
		'name' => __('Copyright Text', 'options_framework_theme'),
		'desc' => __('Copyright Text', 'options_framework_theme'),
		'id' => 'copyright_text',
		'placeholder' => 'Enter Copyright Text',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Footer Copyright Text', 'options_framework_theme'),
		'desc' => __('Footer Copyright Text', 'options_framework_theme'),
		'id' => 'footer_about_txt',
		'placeholder' => 'Footer Copyright Text',
		'type' => 'textarea');
		
	
	$options[] = array(
		'name' => __('Social', 'options_framework_theme'),
		'type' => 'heading',
		'icon' => 'share-alt');
	
	$options[] = array(
		'name' => __('Facebook URL', 'options_framework_theme'),
		'desc' => __('Facebook URL', 'options_framework_theme'),
		'id' => 'social_fb',
		'placeholder' => 'Facebook Page URL',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Twitter URL', 'options_framework_theme'),
		'desc' => __('Twitter URL', 'options_framework_theme'),
		'id' => 'social_twitter',
		'placeholder' => 'Twitter Page URL',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Linkedin URL', 'options_framework_theme'),
		'desc' => __('Linkedin URL', 'options_framework_theme'),
		'id' => 'social_linkedin',
		'placeholder' => 'Linkedin Page URL',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Youtube URL', 'options_framework_theme'),
		'desc' => __('Youtube URL', 'options_framework_theme'),
		'id' => 'social_youtube',
		'placeholder' => 'Youtube Page URL',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Google Plus URL', 'options_framework_theme'),
		'desc' => __('Google Plus URL', 'options_framework_theme'),
		'id' => 'social_gplus',
		'placeholder' => 'Google Plus Page URL',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Pintrest URL', 'options_framework_theme'),
		'desc' => __('Pintrest URL', 'options_framework_theme'),
		'id' => 'social_pintrest',
		'placeholder' => 'Pintrest Page URL',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Instagram URL', 'options_framework_theme'),
		'desc' => __('Instagram URL', 'options_framework_theme'),
		'id' => 'social_instagram',
		'placeholder' => 'Instagram Page URL',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Homepage', 'options_framework_theme'),
		'type' => 'heading',
		'icon' => 'home');

	$options[] = array(
		'name' => __('Homepage Banner', 'options_framework_theme'),
		'desc' => __('Upload Homepage Banner', 'options_framework_theme'),
		'id' => 'homepage_banner_img',
		'type' => 'upload');
		
	$options[] = array(
		'class' => 'section-divider',
		'type' => 'Hrule');
		
	$options[] = array(
		'name' => __('Homepage Banner Text', 'options_framework_theme'),
		'desc' => __('Enter Homepage Banner Text', 'options_framework_theme'),
		'id' => 'homepage_banner_txt',
		'type' => 'editor');
		
	$options[] = array(
		'name' => __('Promo Text One', 'options_framework_theme'),
		'desc' => __('Promo Text One', 'options_framework_theme'),
		'id' => 'promo_txt_one_homepage',
		'placeholder' => 'Enter Homepage Promo Text One',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Promo Text Two', 'options_framework_theme'),
		'desc' => __('Promo Text Two', 'options_framework_theme'),
		'id' => 'promo_txt_two_homepage',
		'placeholder' => 'Enter Homepage Promo Text Two',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Homepage Services Image', 'options_framework_theme'),
		'desc' => __('Homepage Services Image', 'options_framework_theme'),
		'id' => 'hme_services_image',
		'placeholder' => 'Upload Homepage Services Image',
		'type' => 'upload');
		
	$options[] = array(
		'name' => __('Homepage Book Order Image', 'options_framework_theme'),
		'desc' => __('Homepage Book Order Image', 'options_framework_theme'),
		'id' => 'hme_bookorder_image',
		'placeholder' => 'Upload Homepage Book Order Image',
		'type' => 'upload');
		
	$options[] = array(
		'name' => __('Homepage Book Order Text', 'options_framework_theme'),
		'desc' => __('Enter Homepage Book Order Text', 'options_framework_theme'),
		'id' => 'hme_bookorder_txt',
		'type' => 'editor');
		
	return $options;
}
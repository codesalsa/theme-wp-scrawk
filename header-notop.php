<?php
/**
 * The custom page header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package scrawk
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />



<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

 
<?php if ( of_get_option( 'site_description' ) ) { ?>
	<meta name="description" content="<?php echo of_get_option( 'site_description' ); ?>">
<?php } ?>
    
<?php if ( of_get_option( 'site_keywords' ) ) { ?>
	<meta name="keywords" content="<?php echo of_get_option( 'site_keywords' ); ?>">
<?php } ?>

<?php if ( of_get_option( 'favicon_uploader' ) ) { ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo of_get_option( 'favicon_uploader' ); ?>">
<?php } ?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

		<header id="main-header-notop" class="full-width clearfix nav-fixed-top" data-bg="brown-black">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 branding-container">
							<a class="branding" href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<?php if ( of_get_option('logo_uploader') ) { ?> 

									<img src="<?php echo of_get_option('logo_uploader'); ?>" class="img-responsive" />

								<?php } ?>
							</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 header-right">
							<nav class="primary-nav">
								<?php wp_nav_menu( array( 'items_wrap' => '<ul class="hidden-xs hidden-sm">%3$s</ul>', 'theme_location' => 'primary') ); ?>
							</nav>
						</div>
					</div>
				</div>
			</header>
			<section class="mobile-menu-cont hidden-lg hidden-md">
				<div id="menu-ham" class="hamburger hamburger--elastic hidden-lg hidden-md">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
				<div id="mobile-nav" class="hidden-lg hidden-md">
					<?php wp_nav_menu( array( 'items_wrap' => '<ul>%3$s</ul>', 'theme_location' => 'mobilemenu') ); ?>
				</div>
			</section>
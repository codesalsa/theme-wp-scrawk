<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package scrawk
 */

get_header();
?>

<section id="services-container" class="full-width clearfix" data-bg="gray">
	<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<h2 class="section-title">What can we do for you?</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<?php 
									query_posts(array( 
										'order' => 'DESC',
										'post_type' => 'servicescpt',
										'showposts' => 6,
									) );  
								?>
								<?php while ( have_posts() ) : the_post(); ?>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 w-item ms">
										
										<?php
											$iconType = get_field('service_icon_type');
											$icon = get_field('service_icon');

											if($iconType == 'FontAwesome Icon'): ?>
												<div class="service-icon fa-icon">
													<i class="fa <?php echo $icon; ?>"></i>
												</div>
											<?php elseif($iconType == 'Icon Image'): ?>
												<div class="service-icon img-icon">
													<img src="<?php echo $icon; ?>" class="img-responsive" />
												</div>
											<?php endif; ?>
										<a href="<?php the_permalink(); ?>">
										<h5><?php the_title(); ?></h5>
										<p><?php echo excerpt(15); ?></p>
										</a>
									</div>
								<?php endwhile; ?>
								<?php wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<img src="<?php echo of_get_option('hme_services_image'); ?>" class="img-responsive" />
				</div>
			</div>
		</div>
</section>

<section id="testimonials-container" class="full-width clearfix" data-bg="white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">What our customers say</h2>
				<p class="sub-txt"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Mauris elementum mauris vitae tortor. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci.</p>
			</div>
		</div>
		<div class="row testimonials">
			<?php 
				query_posts(array( 
					'order' => 'DESC',
					'post_type' => 'testimonialscpt',
					'showposts' => 3,
				) );  
			?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 testi-items">
					<?php if(has_post_thumbnail()): ?>	
						<div class="testi-img">
							<?php the_post_thumbnail(); ?>
						</div>
					<?php endif; ?>
					<div class="blockquote-wrap text-center">
						<blockquote class="quotes">
							<p><?php echo excerpt(15); ?></p>
							<div class="block-quote-meta">
								<span class="client-name"><?php the_title(); ?></span>
								<small class="designation">
									<?php echo get_field('client_designation'); ?>
								</small>
								<span class="company">
									<?php echo get_field('client_company'); ?>
								</span>
								<span class="rating">
									<?php for($x = 1; $x <= get_field('client_rating'); $x++){
										echo "<i class='fa fa-star'></i>";
									} ?>
								</span>
							</div>
							
						</blockquote>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="teams-container" class="full-width clearfix" data-bg = "light-gray">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">Meet our Team</h2>
				<p class="sub-txt"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Mauris elementum mauris vitae tortor. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci.</p>
			</div>
		</div>
		<div class="row teams">
			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 teams-list">
				<div class="row">
					<?php 
						query_posts(array( 
							'order' => 'DESC',
							'post_type' => 'teamscpt',
							'showposts' => 4,
						) );  
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php 
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); 
						$member_socials = get_field('member_social_profile'); 
					?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 team-item">
						<a href="#" data-member-image="<?php echo $thumbnail[0]; ?>" data-member-name="<?php the_title(); ?>" data-member-content="<?php the_content(); ?>" data-member-designation="<?php echo get_field('member_designation'); ?>" data-member-social='{ "fb" : "<?php echo $member_socials['facebook_url']; ?>", "twitter" : "<?php echo $member_socials['twitter_url']; ?>", "linkedin" : "<?php echo $member_socials['linkedin_url']; ?>", "gplus" : "<?php echo $member_socials['google_plus_url']; ?>"}'  class="team-mem-item">
							<?php if(has_post_thumbnail()): ?>	
								<div class="team-item-img">
									<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
								</div>
							<?php endif; ?>
							<span class="team-item-title"><?php the_title(); ?></span>
						</a>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 team-item-details">
				<?php 
					query_posts(array( 
						'order' => 'DESC',
						'showposts' => 1,
						'post_type' => 'teamscpt',
					) );  
				?>
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="show-team-details">
						<?php if(has_post_thumbnail()): ?>	
							<div class="team-item-img">
								<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
							</div>
						<?php endif; ?>
						<div class="team-item-details-cont">
							<span class="team-item-title"><?php the_title(); ?></span>
							<span class="team-item-desig">
								<?php echo get_field('member_designation'); ?>
							</span>
							<div class="team-item-detail-txt">
								<?php the_content(); ?>
							</div>
							<?php 
								$member_socials = get_field('member_social_profile'); 
								if( $member_socials ):
							?>
							<div class="team-item-socials">
								<?php if($member_socials['facebook_url']): ?>
									<a href="<?php echo $member_socials['facebook_url']; ?>" class="fb-profile"><i class="fa fa-facebook"></i></a>
								<?php endif; ?>
								<?php if($member_socials['twitter_url']): ?>
									<a href="<?php echo $member_socials['twitter_url']; ?>" class="twitter-profile"><i class="fa fa-twitter"></i></a>
								<?php endif; ?>
								<?php if($member_socials['linkedin_url']): ?>
									<a href="<?php echo $member_socials['linkedin_url']; ?> class="linkedin-profile""><i class="fa fa-linkedin"></i></a>
								<?php endif; ?>
								<?php if($member_socials['google_plus_url']): ?>
									<a href="<?php echo $member_socials['google_plus_url']; ?>" class="gplus-profile"><i class="fa fa-google-plus"></i></a>
								<?php endif; ?>

							</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</div>

		</div>
	</div>
</section>

<section class="promo-container full-width clearfix" data-bg = "black">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h3>Scrawk is a great Business wordpress template</h3>
				<p class="sub-txt"> Nullam faucibus mi quis velit. Etiam quis quam. Mauris metus. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Ut tempus purus at lorem. Nullam dapibus fermentum ipsum. Duis condimentum augue id magna semper rutrum. Nulla pulvinar eleifend sem.</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<a href="#" class="btn base-btn">Purchase</a>
			</div>
		</div>
	</div>
</section>

<section id="quote-container" class="full-width clearfix" data-bg = "light-gray">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						<h2 class="section-title">Request a Quote!</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-form">
						<?php echo do_shortcode("[contact-form-7 id='96' title='Request a Quote']"); ?>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						<h2 class="section-title">Frequently Asked!</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						
						<div class="panel-group" id="faq-collapsible">
							<div class="panel panel-default">
								<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										Do i need a business plan?
									</a>
								</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										How long should a business plan be?
									</a>
								</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										What goes into a business plan?
									</a>
								</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
										<div class="media">
										<a class="pull-left" href="#">
											<img class="media-object" src="https://www.placehold.it/150.png" alt="Placeholder">
										</a>
										<div class="media-body">
											<h4 class="media-heading">Filme, die Bewegtes bewegen</h4>
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
										</div>
									</div>
								</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										Where do i start?
									</a>
								</h4>
								</div>
								<div id="collapseFour" class="panel-collapse collapse">
								<div class="panel-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="partners-container" class="full-width clearfix" data-bg = "white">
	<div class="container" data-padding="10015">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
				<h2 class="section-title">They help us changing the face of Business</h2>
				<p class="sub-txt"> Demonstr averunt lectores legere me lius quod qua pro legunt saepius Claritas est etiam pro cessus dynamicus qui sequitur mutationem consuetudium lec torum. Mirum notare quam littera gothica, quam the nunc.</p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row partners">
					<?php 
						query_posts(array( 
							'order' => 'DESC',
							'showposts' => 6,
							'post_type' => 'partnerscpt',
						) );  
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 partner-item">
						<?php
							$image = get_field('partner_logo');

							if( !empty($image) ): ?>
							<a href="<?php echo get_field('partner_url'); ?>" class="partner-logo">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						<?php endif; ?>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="blogs-container" class="full-width clearfix" data-bg = "light-gray">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">Recent Blog</h2>
				<p class="sub-txt">Demonstr averunt lectores legere me lius quod qua pro legunt saepius Claritas est etiam pro cessus dynamicus qui sequitur mutationem consuetudium lec torum. Mirum notare quam littera gothica, quam the nunc.</p>
			</div>
		</div>
		<div class="row blog-posts">
			<?php 
				query_posts(array( 
					'order' => 'DESC',
					'showposts' => 3,
					'category_name' => 'blog'
				) );  
			?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<article class="blog-items">
					<?php if(has_post_thumbnail()): ?>	
						<div class="blog-img">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?></a>
						</div>
					<?php endif; ?>
					<div class="blog-content">
						<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						<div class="blog-meta">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><i class="fa fa-user"></i>by <?php the_author(); ?> </a>
							<a><i class="fa fa-calendar"></i><?php echo get_the_date('M Y'); ?></a>
							<?php if(wp_count_comments($post->ID)->total_comments <= 1) { $comments_count = "No Comments"; } else{ $comments_count = wp_count_comments($post->ID)->total_comments; } ?>
							<a><i class="fa fa-comment"></i><?php echo $comments_count; ?> </a>
						</div>
						<p><?php echo excerpt(25); ?></p>
						<a href="<?php the_permalink(); ?>" class="btn btn-blog">read more
							<i class="fa fa-long-arrow-right"></i>
						</a>
					</div>
				</article>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="about-container" class="full-width clearfix" data-bg="white">
	<div class="container-fluid">
		<div class="row">
			<?php
			    $query = new WP_Query( 'pagename=about-us' );
				while ( $query->have_posts() ) : $query->the_post();
				if ( has_post_thumbnail() ):
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				endif;
			?>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left info-cont">
				<h2 class="section-title"><?php the_title(); ?></h2>
				<p class="sub-txt"><?php echo excerpt(65); ?></p>
				<a href="<?php the_permalink(); ?>" class="btn base-btn">Learn More</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" <?php if ( has_post_thumbnail() ): ?> style="background: url('<?php echo $thumb['0']; ?>') no-repeat center center;background-size:cover;" <?php endif; ?>>

			</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

	
<?php
get_footer();

jQuery(function($) {
  jQuery(document).ready(function() {
    jQuery(this).scrollTop(0);
  });
  var shrinkHeader = 50;

  jQuery(window).scroll(function() {
    var scroll = getCurrentScroll();
    if (scroll >= shrinkHeader) {
      jQuery("#main-header").addClass("shrink");
    } else {
      jQuery("#main-header").removeClass("shrink");
    }
  });
  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }

  jQuery("#menu-ham").click(function() {
    jQuery(this).toggleClass("is-active");

    jQuery("#mobile-nav").toggleClass("is-active");
  });

  jQuery("#mobile-nav li.menu-item-has-children a").click(function() {
    jQuery("#mobile-nav .sub-menu").hide();
    jQuery(this)
      .parent("li")
      .children(".sub-menu")
      .toggle();
  });

  jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    jQuery("#header-bg-section").css(
      "transform",
      "translateY(" + scroll / 2 + "px)"
    );
  });

  jQuery(".team-mem-item").on("click", function(e) {
    e.preventDefault();
    var member_designation = jQuery(this).data("member-designation");
    var member_name = jQuery(this).data("member-name");
    var member_content = jQuery(this).data("member-content");
    var member_social = jQuery(this).data("member-social");
    var member_image = jQuery(this).data("member-image");

    var member_social_val = jQuery.parseJSON(JSON.stringify(member_social));
    //alert(member_social_val);

    jQuery(".show-team-details .team-item-detail-txt").html(member_content);
    jQuery(".show-team-details .team-item-desig").html(member_designation);
    jQuery(".show-team-details .team-item-title").html(member_name);

    jQuery(".show-team-details .team-item-img img")
      .attr("srcset", member_image)
      .attr("src", member_image);

    jQuery(".team-item-socials").html("");

    if (member_social_val.fb != "") {
      jQuery(".team-item-socials").append(
        "<a href=" +
          member_social_val.fb +
          "><span class='fa fa-facebook'></span></a>"
      );
    }

    if (member_social_val.twitter != "") {
      jQuery(".team-item-socials").append(
        "<a href=" +
          member_social_val.twitter +
          "><span class='fa fa-twitter'></span></a>"
      );
    }

    if (member_social_val.linkedin != "") {
      jQuery(".team-item-socials").append(
        "<a href=" +
          member_social_val.linkedin +
          "><span class='fa fa-linkedin'></span></a>"
      );
    }
    if (member_social_val.gplus != "") {
      jQuery(".team-item-socials").append(
        "<a href=" +
          member_social_val.gplus +
          "><span class='fa fa-google-plus'></span></a>"
      );
    }
  });
});
